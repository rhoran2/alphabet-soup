#  Word Search Application

## Introduction

This application is used to automatically search for words in a scrambled plan. 
This application can search for words going forwards, backwards, up, down, and 
diagonally in every direction. 

## Input

This application expects an input txt file that where the first line must be the 
size of the word search grid. The next lines must contain the word search grid. 
The next lines must contain the words to be searched for. 

### Example Input
```
3x3
A B C
D E F
G H I
ABC
AEI
```

## Output 

This application will output in the console the found words, starting index in the 
word search grid, and the ending index in the word search grid. 

### Example Output
```
ABC 0:0 0:2
AEI 0:0 2:2
```

## Running This Application

This project used maven to build a jar file. The two necessary commands to build and run
this project are as follows: 
```
$mvn clean install
$java -jar target/word-search-1.0-SNAPSHOT.jar {YourInputFile}.txt
```

Please note that your exact syntax may vary depending on any changes made to the POM.xml file.