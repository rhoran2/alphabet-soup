package com.rhoran2.app;

import java.io.IOException;

/**
 * This is the main driver program for the WordSearch application
 */
public class Main {

    public static void main(String[] args) {
        try {
            WordSearch soup = new WordSearch(args[0]);
            soup.searchForWords();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
