package com.rhoran2.app;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Class that contains all of the functions necessary to read the input file that contains
 * the word search scramble, read the words to be searched, search for the words, and output
 * the results.
 */
public class WordSearch {

    private int height;
    private int width;
    private final char[][] board;
    private final List<String> searchWords;
    private Point foundWordStart;
    private Point foundWordEnd;
    private static final Logger LOGGER = Logger.getLogger(WordSearch.class.getName());

    /**
     * Constructor for the WordSearch class. This contains functionality for reading the
     * input file and setting up the necessary variables.
     * @param fileLocation location of the input file containing the word search scramble
     *                     and the words to be searched.
     * @throws IOException
     */
    public WordSearch(String fileLocation) throws IOException {
        searchWords = new ArrayList<>();
        foundWordStart = new Point();
        foundWordEnd = new Point();
        LOGGER.fine("Reading input file: " + fileLocation);
        try (BufferedReader br = new BufferedReader(new FileReader(fileLocation))) {
            String size[] = br.readLine().split("x");
            height = Integer.parseInt(size[0]);
            width = Integer.parseInt(size[1]);
            LOGGER.fine("Reading word search grid.");
            board = new char[height][width];
            String readLine;
            for (int i = 0; i < height; i++) {
                readLine = br.readLine();
                if (readLine != null) {
                    board[i] = readLine.replace(" ", "").toCharArray();
                } else {
                    System.err.println("Grid size not accurate");
                }
            }
            LOGGER.fine("Reading words to be searched.");
            readLine = br.readLine();
            while (readLine != null) {
                searchWords.add(readLine);
                readLine = br.readLine();
            }
        }
    }

    /**
     * This is the main calling method of the WordSearch class. This searches for the first
     * of all of the words.
     */
    public void searchForWords() {
        for (String searchWord : searchWords) {
            LOGGER.fine("Searching for " + searchWord);
            char searchChar = searchWord.charAt(0);
            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    if (board[i][j] == searchChar) {
                        foundWordStart.x = i;
                        foundWordStart.y = j;
                        LOGGER.fine("Possibly found " + searchWord);
                        if (checkDirection(searchWord)) {
                            LOGGER.fine("Found " + searchWord + ". Generating output.");
                            System.out.println(searchWord + " " + foundWordStart.x + ":" + foundWordStart.y +
                                    " " + foundWordEnd.x + ":" + foundWordEnd.y);
                            /*
                            Reset the found word Points
                             */
                            foundWordEnd = new Point();
                            foundWordStart = new Point();
                        }
                        else {
                            LOGGER.fine("Did not find it. Trying again.");
                        }
                    }
                }
            }
        }
    }

    /**
     * This method searches for the second letter of the word being searched in every direction
     * possible. Once a possible direction is determined the rest of the word is searched for
     * in that direction.
     * @param searchWord
     * @return
     */
    private boolean checkDirection(String searchWord) {
        char nextChar = searchWord.charAt(1);
        int searchWordLength = searchWord.length() - 1;
        /*
        Ignore IndexOutOfBoundsException for cases of the first letter being on the edge/corner
         */
        try {
            if (board[foundWordStart.x + 1][foundWordStart.y] == nextChar) {
                foundWordEnd = new Point(foundWordStart.x + searchWordLength, foundWordStart.y);
                if (findWordRight(foundWordStart, foundWordEnd, searchWord, searchWordLength)) {
                    return true;
                }
            }
        } catch (IndexOutOfBoundsException ignored) {
        }
        try {
            if (board[foundWordStart.x - 1][foundWordStart.y] == nextChar) {
                foundWordEnd = new Point(foundWordStart.x - searchWordLength, foundWordStart.y);
                if (findWordLeft(foundWordStart, foundWordEnd, searchWord, searchWordLength)) {
                    return true;
                }
            }
        } catch (IndexOutOfBoundsException ignored) {
        }
        try {
            if (board[foundWordStart.x][foundWordStart.y - 1] == nextChar) {
                foundWordEnd = new Point(foundWordStart.x, foundWordStart.y - searchWordLength);
                if (findWordUp(foundWordStart, foundWordEnd, searchWord, searchWordLength)) {
                    return true;
                }
            }
        } catch (IndexOutOfBoundsException ignored) {
        }
        try {
            if (board[foundWordStart.x][foundWordStart.y + 1] == nextChar) {
                foundWordEnd = new Point(foundWordStart.x, foundWordStart.y + searchWordLength);
                if (findWordDown(foundWordStart, foundWordEnd, searchWord, searchWordLength)) {
                    return true;
                }
            }
        } catch (IndexOutOfBoundsException ignored) {
        }
        try {
            if (board[foundWordStart.x + 1][foundWordStart.y - 1] == nextChar) {
                foundWordEnd = new Point(foundWordStart.x + searchWordLength, foundWordStart.y - searchWordLength);
                if (findWordUpRight(foundWordStart, foundWordEnd, searchWord, searchWordLength)) {
                    return true;
                }
            }
        } catch (IndexOutOfBoundsException ignored) {
        }
        try {
            if (board[foundWordStart.x - 1][foundWordStart.y - 1] == nextChar) {
                foundWordEnd = new Point(foundWordStart.x - searchWordLength, foundWordStart.y - searchWordLength);
                if (findWordUpLeft(foundWordStart, foundWordEnd, searchWord, searchWordLength)) {
                    return true;
                }
            }
        } catch (IndexOutOfBoundsException ignored) {
        }
        try {
            if (board[foundWordStart.x + 1][foundWordStart.y + 1] == nextChar) {
                foundWordEnd = new Point(foundWordStart.x + searchWordLength, foundWordStart.y + searchWordLength);
                if (findWordDownRight(foundWordStart, foundWordEnd, searchWord, searchWordLength)) {
                    return true;
                }
            }
        } catch (IndexOutOfBoundsException ignored) {
        }
        try {
            if (board[foundWordStart.x - 1][foundWordStart.y + 1] == nextChar) {
                foundWordEnd = new Point(foundWordStart.x - searchWordLength, foundWordStart.y + searchWordLength);
                if (findWordDownLeft(foundWordStart, foundWordEnd, searchWord, searchWordLength)) {
                    return true;
                }
            }
        } catch (IndexOutOfBoundsException ignored) {
        }
        foundWordStart = new Point();
        foundWordEnd = new Point();
        return false;
    }

    /**
     * Recursive method used for searching for a word in the right direction.
     * @param start
     * @param end
     * @param searchWord
     * @param index
     * @return
     */
    private boolean findWordRight(Point start, Point end, String searchWord, int index) {
        if (end.y > width) {
            return false;
        }
        if (start.x > end.x && index < 0) {
            return true;
        }
        if (board[end.x][end.y] == searchWord.charAt(index)) {
            return findWordRight(start, new Point(end.x - 1, end.y), searchWord, index - 1);
        }
        return false;
    }

    /**
     * Recursive method used for searching for a word in the left direction.
     * @param start
     * @param end
     * @param searchWord
     * @param index
     * @return
     */
    private boolean findWordLeft(Point start, Point end, String searchWord, int index) {
        if (end.y < 0) {
            return false;
        }
        if (start.x < end.x && index < 0) {
            return true;
        }
        if(board[end.x][end.y] == searchWord.charAt(index)) {
            return findWordLeft(start, new Point(end.x + 1, end.y), searchWord, index - 1);
        }
        return false;
    }

    /**
     * Recursive method used for searching for a word in the upward direction.
     * @param start
     * @param end
     * @param searchWord
     * @param index
     * @return
     */
    private boolean findWordUp(Point start, Point end, String searchWord, int index) {
        if (end.x < 0) {
            return false;
        }
        if (start.y < end.y && index < 0) {
            return true;
        }
        if (board[end.x][end.y] == searchWord.charAt(index)) {
            return findWordUp(start, new Point(end.x, end.y + 1), searchWord, index - 1);
        }
        return false;
    }

    /**
     * Recursive method used for searching for a word in the downward direction.
     * @param start
     * @param end
     * @param searchWord
     * @param index
     * @return
     */
    private boolean findWordDown(Point start, Point end, String searchWord, int index) {
        if (end.x > height) {
            return false;
        }
        if (start.y > end.y && index < 0) {
            return true;
        }
        if (board[end.x][end.y] == searchWord.charAt(index)) {
        return findWordDown(start, new Point(end.x, end.y - 1), searchWord, index - 1);
        }
        return false;
    }

    /**
     * Recursive method used for searching for a word diagonally going up and to the right.
     * @param start
     * @param end
     * @param searchWord
     * @param index
     * @return
     */
    private boolean findWordUpRight(Point start, Point end, String searchWord, int index) {
        if (end.x < 0 || end.y > width) {
            return false;
        }
        if (start.x > end.x && start.y < end.y && index < 0) {
            return true;
        }
        if (board[end.x][end.y] == searchWord.charAt(index)) {
            return findWordUpRight(start, new Point(end.x - 1, end.y + 1), searchWord, index - 1);
        }
        return false;
    }

    /**
     * Recursive method used for searching for a word diagonally going up and to the left.
     * @param start
     * @param end
     * @param searchWord
     * @param index
     * @return
     */
    private boolean findWordUpLeft(Point start, Point end, String searchWord, int index) {
        if (end.x < 0 || end.y < 0) {
            return false;
        }
        if (start.x < end.x && start.y < end.y && index < 0) {
            return true;
        }
        if (board[end.x][end.y] == searchWord.charAt(index)) {
            return findWordUpLeft(start, new Point(end.x + 1, end.y + 1), searchWord, index - 1);
        }
        return false;
    }

    /**
     * Recursive method used for searching for a word diagonally going down and to the right.
     * @param start
     * @param end
     * @param searchWord
     * @param index
     * @return
     */
    private boolean findWordDownRight(Point start, Point end, String searchWord, int index) {
        if (end.x > height || end.y > width) {
            return false;
        }
        if (start.x > end.x && start.y > end.y && index < 0) {
            return true;
        }
        if (board[end.x][end.y] == searchWord.charAt(index)) {
            return findWordDownRight(start, new Point(end.x - 1, end.y - 1), searchWord, index - 1);
        }
        return false;
    }

    /**
     * Recursive method used for searching for a word diagonally going down and to the left.
     * @param start
     * @param end
     * @param searchWord
     * @param index
     * @return
     */
    private boolean findWordDownLeft(Point start, Point end, String searchWord, int index) {
        if (end.x > height || end.y < 0) {
            return false;
        }
        if (start.x < end.x && start.y > end.y && index < 0) {
            return true;
        }
        if (board[end.x][end.y] == searchWord.charAt(index)) {
            return findWordDownLeft(start, new Point(end.x + 1, end.y - 1), searchWord, index - 1);
        }
        return false;
    }
}
