package com.rhoran2.app;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

/**
 * Test class that executes a given input file and verifies the expected resulting
 * output in the console.
 */
public class WordSearchTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private WordSearch wordSearch;

    @Before
    public void setUpStreams() throws IOException {
        wordSearch = new WordSearch("input.txt");
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test
    public void searchForChar() {
        wordSearch.searchForWords();
        Assert.assertEquals("HELLO 0:0 4:4\nGOOD 4:0 4:3\nBYE 1:3 1:1\n", outContent.toString());
    }
}